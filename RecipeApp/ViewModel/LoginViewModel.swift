//
//  LoginViewModel.swift
//  RecipeApp
//
//  Created by Calvin on 29/05/2022.
//

import Foundation
import FirebaseAuth

class LoginViewModel : NSObject {
    
    func createUser(email: String, password: String, completion: @escaping (Bool, String) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) {(authResult, error) in
            var errorMessage = String()
            if let error = error {
                let retrievedError = error as NSError
                switch retrievedError.code {
                case AuthErrorCode.emailAlreadyInUse.rawValue:
                    errorMessage = "Email already in use"
                case AuthErrorCode.weakPassword.rawValue:
                    errorMessage = "Please provide a stronger password"
                default:
                    errorMessage = "Unable to create user"
                }
            }
            if let user = authResult?.user {
                print(user)
                completion(true, errorMessage)
            } else {
                completion(false, errorMessage)
            }
        }
    }
    
    func signIn(email: String, password: String, completion: @escaping (Bool, String) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) {(authResult, error) in
            var errorMessage = String()
            if let error = error {
                let retrievedError = error as NSError
                switch retrievedError.code {
                case AuthErrorCode.userDisabled.rawValue:
                    errorMessage = "Account has been disabled"
                case AuthErrorCode.invalidEmail.rawValue:
                    errorMessage = "Invalid email"
                case AuthErrorCode.wrongPassword.rawValue:
                    errorMessage = "Wrong password provided"
                default:
                    errorMessage = error.localizedDescription 
                }
            }
            if let user = authResult?.user {
                print(user)
                completion(true, errorMessage)
            } else {
                completion(false, errorMessage)
            }
        }
    }
    
    func validateEmail(email: String, completion: @escaping (Bool) -> Void) {
        Auth.auth().fetchSignInMethods(forEmail: email, completion: { (signInMethods, error) in
            if signInMethods == nil {
                completion(false)
            }
            else {
                print(signInMethods)
                completion(true)
            }
        })
    }
}
