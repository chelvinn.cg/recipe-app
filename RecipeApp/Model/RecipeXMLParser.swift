//
//  XMLParser.swift
//  RecipeApp
//
//  Created by Calvin on 28/05/2022.
//

import Foundation

class RecipeXMLParser: NSObject, XMLParserDelegate
{
    private var recipes = [Recipe]()
    private var currentRecipe = Recipe()
    private var currentElement = String()
    private var parserCompletionHandler: (([Recipe]) -> Void)?
    private var xmlText = String()
    
    func parseFeed(url: String, completionHandler: (([Recipe]) -> Void)?)
    {
        self.parserCompletionHandler = completionHandler
        
        let request = URLRequest(url: URL(string: url)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                if let error = error {
                    print(error.localizedDescription)
                }
                
                return
            }
            
            let parser = XMLParser(data: data)
            parser.delegate = self
            parser.parse()
        }
        
        task.resume()
    }
    
    // MARK: - XML Parser Delegate
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:])
    {
        self.currentElement = elementName
        self.xmlText = ""
        if self.currentElement == Constant.XMLElement.recipe.rawValue {
            self.currentRecipe = Recipe()
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String)
    {
        self.xmlText += string
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?)
    {
        if elementName == Constant.XMLElement.id.rawValue {
            self.currentRecipe.id = self.xmlText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        if elementName == Constant.XMLElement.title.rawValue {
            self.currentRecipe.title = self.xmlText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        if elementName == Constant.XMLElement.category.rawValue {
            self.currentRecipe.category = self.xmlText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        if elementName == Constant.XMLElement.ingredient.rawValue {
            self.currentRecipe.ingredient = self.xmlText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        if elementName == Constant.XMLElement.steps.rawValue {
            self.currentRecipe.steps = self.xmlText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        if elementName == Constant.XMLElement.imageURL.rawValue {
            self.currentRecipe.imageURL = self.xmlText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        if elementName == Constant.XMLElement.recipe.rawValue {
            self.recipes.append(self.currentRecipe)
        }
    }

    func parserDidEndDocument(_ parser: XMLParser) {
        self.parserCompletionHandler?(self.recipes)
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error)
    {
        print(parseError.localizedDescription)
    }
    
}
