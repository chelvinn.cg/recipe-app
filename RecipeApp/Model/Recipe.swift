//
//  Recipe.swift
//  RecipeApp
//
//  Created by Calvin on 28/05/2022.
//

import Foundation
import RealmSwift

class Recipe: Object, Codable {
    @objc dynamic var id : String?
    @objc dynamic var title: String?
    @objc dynamic var category: String?
    @objc dynamic var ingredient: String?
    @objc dynamic var steps: String?
    @objc dynamic var imageURL: String?
}
