//
//  File.swift
//  RecipeApp
//
//  Created by Calvin on 28/05/2022.
//

import Foundation

enum Constant {
    enum XMLElement: String {
        case recipe = "recipe"
        case id = "id"
        case title = "title"
        case category = "category"
        case ingredient = "ingredient"
        case steps = "steps"
        case imageURL = "imageURL"
    }
    
    enum RecipeCategory: String {
        case all = "All Categories"
    }
    
    enum UserDefaults: String {
        case userDefaultsEmail
        case userDefaultsIsLoggedIn
    }
}
