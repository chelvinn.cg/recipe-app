//
//  AppHelper.swift
//  RecipeApp
//
//  Created by Calvin on 28/05/2022.
//

import Foundation
import Kingfisher
import SwiftMessages
import RealmSwift

class AppHelper {
    /// Function used to set image to an image view
    static func setImage(fromURL: String?, toImageView: UIImageView, placeholder: String) {
        var url: URL?
        if let imageURL = fromURL {
            let synthesizedURL = imageURL.replacingOccurrences(of: " ", with: "%20")
            url = URL(string: synthesizedURL)
        }
        toImageView.kf.setImage(with: url, placeholder: UIImage(named: placeholder), options: [.transition(.fade(0.3))])
    }
    
    /// Function used to show "info" toast message to user
    static func showFailToast(title: String = "Message", message: String, position: SwiftMessages.PresentationStyle = .bottom) {
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(.error)
        view.button?.isHidden = true
        view.configureDropShadow()
        view.configureContent(title: title, body: message)
        
        var config = SwiftMessages.Config()
        config.presentationStyle = position
        config.duration = .seconds(seconds: 2.0)
        config.dimMode = .gray(interactive: true)
        config.preferredStatusBarStyle = .darkContent
        
        SwiftMessages.show(config: config, view: view)
        return
    }
    
    static func insertObjectArrayToRealm<T: RealmSwift.Object>(fromArray array: [T]) {
        let realm = try! Realm()
        for eachObject in array {
            try! realm.write {
                realm.add(eachObject)
            }
        }
    }
    
    static func insertObjectToRealm<T: RealmSwift.Object>(fromObject object: T) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(object)
        }
    }
    
    static func deleteObjectFromRealm<T: RealmSwift.Object>(withName: Results<T>) {
        let realm = try! Realm()
        if !withName.isEmpty {
            try! realm.write {
                realm.delete(withName)
            }
        }
    }
    
    static func deleteAllObjectFromRealm() {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    static func saveImage(imageName: String, image: UIImage) {
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }

        let fileName = imageName
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1) else { return }

        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }

        }

        do {
            try data.write(to: fileURL)
        } catch let error {
            print("error saving file with error", error)
        }
    }
    
    static func loadImageFromDiskWith(fileName: String) -> UIImage? {
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)

        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image

        }
        
        return nil
    }
    
    static func validatePassword(_ password: String) -> Bool {
        let minPasswordLength = 6
        return password.count >= minPasswordLength
    }
    
    static func validateEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    static func clearDataOnLogout() {
        UserDefaults.standard.removeObject(forKey: Constant.UserDefaults.userDefaultsEmail.rawValue)
        UserDefaults.standard.set(false, forKey: Constant.UserDefaults.userDefaultsIsLoggedIn.rawValue)
        UserDefaults.standard.synchronize()
    }
}
