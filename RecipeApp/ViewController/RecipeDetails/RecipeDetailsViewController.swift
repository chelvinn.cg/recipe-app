//
//  RecipeDetailsViewController.swift
//  RecipeApp
//
//  Created by Calvin on 28/05/2022.
//

import UIKit
import RealmSwift

class RecipeDetailsViewController: UIViewController {

    /* =================================================================
     *                   MARK: - Local Initialization
     * ================================================================= */
    var recipe = Recipe()
    lazy var realm = {
        return try! Realm()
    }()
    private var defaultImage = UIImage()
    private var isLoggedIn = false
    
    /* =================================================================
     *                   MARK: - Outlet Initialization
     * ================================================================= */
    @IBOutlet weak var recipeImageView: UIImageView!
    @IBOutlet weak var recipeTitleTextField: UITextField!
    @IBOutlet weak var recipeIngredientsTextView: UITextView!
    @IBOutlet weak var recipeStepsTextView: UITextView!
    
    @IBAction func onRecipeImageViewTapped(_ sender: Any) {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        self.present(vc, animated: true, completion: nil)
    }
    
    /* =================================================================
     *                   MARK: - Class Function
     * ================================================================= */
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupView()
    }
    
    @objc func onSaveButtonTapped() {
        self.isLoggedIn = UserDefaults.standard.bool(forKey: Constant.UserDefaults.userDefaultsIsLoggedIn.rawValue)
        
        if self.isLoggedIn {
            let recipeToSave = self.realm.objects(Recipe.self).filter({$0.id == self.recipe.id}).first
            try! realm.write {
                recipeToSave?.title = self.recipeTitleTextField.text
                recipeToSave?.ingredient = self.recipeIngredientsTextView.text
                recipeToSave?.steps = self.recipeStepsTextView.text
                let newImage = self.recipeImageView.image
                if !self.defaultImage.isEqual(newImage) {
                    AppHelper.saveImage(imageName: self.recipe.id!, image: self.recipeImageView.image!)
                    recipeToSave?.imageURL = self.recipe.id!
                }
            }
            NotificationCenter.default.post(name: .onNewRecipeAdded, object: self)
            let alertController = UIAlertController(
                title: "Saved!",
                message: "Successfully saved recipe!",
                preferredStyle: UIAlertController.Style.alert
            )
            let confirmAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { _ in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.recipe = self.realm.objects(Recipe.self).filter({$0.id == self.recipe.id}).first!
                    self.setupView()
                })
            }
            alertController.addAction(confirmAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            self.showLoginToast()
        }
    }
    
    @objc func onDeleteButtonTapped() {
        self.isLoggedIn = UserDefaults.standard.bool(forKey: Constant.UserDefaults.userDefaultsIsLoggedIn.rawValue)
        
        if self.isLoggedIn {
            let alertController = UIAlertController(
                title: "Warning: Deleting Recipe",
                message: "Would you like to proceed?",
                preferredStyle: UIAlertController.Style.alert
            )
            let confirmAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { _ in
                let recipeToDelete = Array(self.realm.objects(Recipe.self).filter({$0.id == self.recipe.id}))
                DispatchQueue.main.async {
                    try! self.realm.write {
                        self.realm.delete(recipeToDelete)
                    }
                }
                self.presentSuccessfullyDeletedRecipeAlertView()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive) { _ in
            }
            alertController.addAction(confirmAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            self.showLoginToast()
        }
    }
    
    private func setupView() {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "Recipe Details"
        
        let saveButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(self.onSaveButtonTapped))
        let deleteButton = UIBarButtonItem(title: "Delete", style: .plain, target: self, action: #selector(self.onDeleteButtonTapped))
        self.navigationItem.rightBarButtonItems = [saveButton, deleteButton]
        
        if let imageURL = self.recipe.imageURL {
            if let url = URL(string: imageURL) {
                if UIApplication.shared.canOpenURL(url) {
                    AppHelper.setImage(fromURL: imageURL, toImageView: self.recipeImageView, placeholder: "ic_placeholder")
                    self.defaultImage = self.recipeImageView.image!
                }
                else {
                    self.recipeImageView.image = AppHelper.loadImageFromDiskWith(fileName: recipe.imageURL!)
                }
            }
        }
        self.recipeTitleTextField.text = self.recipe.title
        self.recipeIngredientsTextView.text = self.recipe.ingredient
        self.recipeStepsTextView.text = self.recipe.steps
        var frame = self.recipeIngredientsTextView.frame
        frame.size.height = self.recipeIngredientsTextView.contentSize.height
        self.recipeIngredientsTextView.frame = frame
        frame.size.height = self.recipeStepsTextView.contentSize.height
        self.recipeStepsTextView.frame = frame
    }
    
    private func presentSuccessfullyDeletedRecipeAlertView() {
        let alertController = UIAlertController(
            title: "Success!",
            message: "Successfully removed recipe.",
            preferredStyle: UIAlertController.Style.alert
        )
        let confirmAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { _ in
            NotificationCenter.default.post(name: .onRecipeDeleted, object: self)
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(confirmAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func showLoginToast() {
        let alertController = UIAlertController(
            title: "Sign in to edit recipe",
            message: "Would you like to proceed to the sign in page?",
            preferredStyle: UIAlertController.Style.alert
        )
        let confirmAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { _ in
            let viewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "loginViewController")
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - UIImagePickerControllerDelegate
extension RecipeDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage {
            self.recipeImageView.image = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
