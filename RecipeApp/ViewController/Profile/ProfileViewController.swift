//
//  ProfileViewController.swift
//  RecipeApp
//
//  Created by Calvin on 29/05/2022.
//

import UIKit
import FirebaseAuth

class ProfileViewController: UIViewController {

    /* =================================================================
     *                   MARK: - Outlet Initialization
     * ================================================================= */
    @IBAction func onLogoutButtonTapped(_ sender: Any) {
        let alertController = UIAlertController(
            title: "Are you sure?",
            message: "You are required to sign in to access the features of this app",
            preferredStyle: UIAlertController.Style.alert
        )
        let confirmAction = UIAlertAction(title: "Confirm", style: UIAlertAction.Style.default) { _ in
            do {
                try Auth.auth().signOut()
                AppHelper.clearDataOnLogout()
                self.navigationController?.popToRootViewController(animated: true)
            }
            catch {
                AppHelper.showFailToast(message: "Unable to sign out")
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    /* =================================================================
     *                   MARK: - Class Function
     * ================================================================= */
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.hidesBackButton = false
        self.navigationItem.title = "Profile"
        self.navigationController?.isNavigationBarHidden = false
    }

}
