//
//  CustomRecipeListingCollectionViewCell.swift
//  RecipeApp
//
//  Created by Calvin on 28/05/2022.
//

import UIKit

class CustomRecipeListingCollectionViewCell: UICollectionViewCell {
    
    /* =================================================================
     *                   MARK: - Local Initialization
     * ================================================================= */
    static let reuseIdentifier = "recipeCell"

    /* =================================================================
     *                   MARK: - Outlet Initialization
     * ================================================================= */
    @IBOutlet weak var recipeImageView: UIImageView!
    @IBOutlet weak var recipeTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
