//
//  MainViewController.swift
//  RecipeApp
//
//  Created by Calvin on 28/05/2022.
//

import UIKit
import RealmSwift

class MainViewController: UIViewController {

    /* =================================================================
     *                   MARK: - Local Initialization
     * ================================================================= */
    private var recipes = [Recipe]()
    private var filteredRecipes = [Recipe]()
    private var selectedCategory = String()
    private var categoryArray = ["All Categories"]
    private let pickerView = UIPickerView()
    private var isLoggedIn = false
    lazy var realm = {
        return try! Realm()
    }()
    
    /* =================================================================
     *                   MARK: - Outlet Initialization
     * ================================================================= */
    @IBOutlet weak var recipeCollectionView: UICollectionView!
    @IBOutlet weak var categoryTextField: UITextField!
    
    @IBAction func onProfileButtonTapped(_ sender: Any) {
        self.isLoggedIn = UserDefaults.standard.bool(forKey: Constant.UserDefaults.userDefaultsIsLoggedIn.rawValue)
        
        if self.isLoggedIn {
            let viewController = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "profileViewController")
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else {
            let alertController = UIAlertController(
                title: "Sign in to view profile",
                message: "Would you like to proceed to the sign in page?",
                preferredStyle: UIAlertController.Style.alert
            )
            let confirmAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { _ in
                let viewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "loginViewController")
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
            alertController.addAction(confirmAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func onAddRecipeButtonTapped(_ sender: Any) {
        self.isLoggedIn = UserDefaults.standard.bool(forKey: Constant.UserDefaults.userDefaultsIsLoggedIn.rawValue)
        
        if self.isLoggedIn {
            let viewController = UIStoryboard(name: "AddRecipe", bundle: nil).instantiateViewController(withIdentifier: "addRecipeViewController")
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else {
            let alertController = UIAlertController(
                title: "Sign in to add recipe",
                message: "Would you like to proceed to the sign in page?",
                preferredStyle: UIAlertController.Style.alert
            )
            let confirmAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { _ in
                let viewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "loginViewController")
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
            alertController.addAction(confirmAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    /* =================================================================
     *                   MARK: - Class Function
     * ================================================================= */
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupView()
        self.setupCollectionView()
        self.retrieveData()
        self.setupRootView()
    }
    
    @objc func onRecipeDeleted() {
        self.filteredRecipes.removeAll()
        self.recipes.removeAll()
        self.categoryArray = ["All Categories"]
        self.recipeCollectionView.reloadData()
        self.retrieveData()
    }
    
    @objc func onNewRecipeAdded() {
        self.recipes.removeAll()
        self.filteredRecipes.removeAll()
        self.recipeCollectionView.reloadData()
        self.retrieveData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.hidesBackButton = true
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func setupCollectionView() {
        self.recipeCollectionView.register(CustomRecipeListingCollectionViewCell.self, forCellWithReuseIdentifier: CustomRecipeListingCollectionViewCell.reuseIdentifier)
        self.recipeCollectionView.register(UINib(nibName: "CustomRecipeListingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CustomRecipeListingCollectionViewCell.reuseIdentifier)
    }
    
    private func setupRootView() {
        NotificationCenter.default.post(name: .onSetNewRootVC, object: self)
    }
    
    private func setupView() {
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.categoryTextField.text = "\(self.categoryArray.first ?? "") \u{25BC}"
        self.categoryTextField.inputView = self.pickerView
        self.categoryTextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(onRecipeDeleted), name: .onRecipeDeleted , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onNewRecipeAdded), name: .onNewRecipeAdded , object: nil)
    }
    
    private func retrieveData() {
        let recipeFromRealm = self.realm.objects(Recipe.self)
        if recipeFromRealm.count == 0 {
            let recipeParser = RecipeXMLParser()
            recipeParser.parseFeed(url: "https://run.mocky.io/v3/0f8ed136-fb21-407c-8a97-b319b22e8aca") { (response) in
                self.recipes = response
                self.filteredRecipes = response
                for recipe in self.recipes {
                    if recipe.category != nil {
                        if !self.categoryArray.contains(recipe.category!) {
                            self.categoryArray.append(recipe.category!)
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.recipeCollectionView.reloadData()
                    AppHelper.deleteAllObjectFromRealm()
                    AppHelper.insertObjectArrayToRealm(fromArray: self.recipes)
                }
            }
        }
        else {
            for recipe in recipeFromRealm {
                if recipe.category != nil {
                    if !self.categoryArray.contains(recipe.category!) {
                        self.categoryArray.append(recipe.category!)
                    }
                }
                self.recipes.append(recipe)
                self.filteredRecipes.append(recipe)
            }
            self.recipeCollectionView.reloadData()
        }
    }
    
    private func filterRecipeByCategory(category: String) {
        if category == Constant.RecipeCategory.all.rawValue {
            self.filteredRecipes = self.recipes
            self.recipeCollectionView.reloadData()
        }
        else {
            self.filteredRecipes = self.recipes.filter({$0.category == category})
            self.recipeCollectionView.reloadData()
        }
    }
}

// MARK: - UICollectionViewDelegate
extension MainViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = UIStoryboard(name: "RecipeDetails", bundle: nil).instantiateViewController(withIdentifier: "recipeDetailsViewController") as! RecipeDetailsViewController
        viewController.recipe = self.filteredRecipes[indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

// MARK: - UICollectionViewDataSource
extension MainViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filteredRecipes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomRecipeListingCollectionViewCell.reuseIdentifier, for: indexPath) as! CustomRecipeListingCollectionViewCell
        
        let recipe = self.filteredRecipes[indexPath.row]
        
        if let imageURL = recipe.imageURL {
            if let url = URL(string: imageURL) {
                if UIApplication.shared.canOpenURL(url) {
                    AppHelper.setImage(fromURL: imageURL, toImageView: cell.recipeImageView, placeholder: "ic_placeholder")
                }
                else {
                    cell.recipeImageView.image = AppHelper.loadImageFromDiskWith(fileName: recipe.imageURL!)
                }
            }
        }
        
        cell.recipeTitle.text = recipe.title

        // Set rounded corners on cell
        cell.contentView.layer.cornerRadius = 10
        cell.contentView.layer.masksToBounds = true
        cell.layer.cornerRadius = 10
        cell.layer.masksToBounds = false
        
        // Set shadow on cell
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.recipeCollectionView.frame.width * 0.9 , height: self.recipeCollectionView.frame.width * 0.9 )
    }
}

// MARK: - UIPickerViewDelegate
extension MainViewController : UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.categoryTextField.text = "\(self.categoryArray[row]) \u{25BC}"
        self.selectedCategory = self.categoryArray[row]
        self.filterRecipeByCategory(category: self.categoryArray[row])
    }
}

// MARK: - UIPickerViewDataSource
extension MainViewController : UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.categoryArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.categoryArray[row]
    }
}

// MARK: - UITextFieldDelegate
extension MainViewController : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.pickerView.reloadAllComponents()
        self.pickerView.delegate?.pickerView?(self.pickerView, didSelectRow: 0, inComponent: 0)
        self.pickerView.selectRow(0, inComponent: 0, animated: false)

        return true
    }
}
