//
//  AddRecipeViewController.swift
//  RecipeApp
//
//  Created by Calvin on 28/05/2022.
//

import UIKit
import IBAnimatable
import RealmSwift

class AddRecipeViewController: UIViewController {

    /* =================================================================
     *                   MARK: - Outlet Initialization
     * ================================================================= */
    @IBOutlet weak var recipeImageView: UIImageView!
    @IBOutlet weak var recipeTitleTextField: UITextField!
    @IBOutlet weak var recipeCategoryTextField: UITextField!
    @IBOutlet weak var recipeIngredientsTextView: AnimatableTextView!
    @IBOutlet weak var recipeStepsTextView: AnimatableTextView!
    lazy var realm = {
        return try! Realm()
    }()
    private var recipeRealm: Results<Recipe> {
        get {
            return realm.objects(Recipe.self)
        }
    }
    
    @IBAction func onRecipeImageTapped(_ sender: Any) {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        self.present(vc, animated: true, completion: nil)
    }
    /* =================================================================
     *                   MARK: - Class Function
     * ================================================================= */
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupView()
    }
    
    @objc func onSaveButtonTapped() {
        if self.recipeTitleTextField.text?.count == 0 {
            AppHelper.showFailToast(message: "Title must not be empty")
            self.recipeTitleTextField.becomeFirstResponder()
        }
        else if self.recipeCategoryTextField.text?.count == 0 {
            AppHelper.showFailToast(message: "Category must not be empty")
            self.recipeCategoryTextField.becomeFirstResponder()
        }
        else if self.recipeIngredientsTextView.text?.count == 0 {
            AppHelper.showFailToast(message: "Ingredient must not be empty")
            self.recipeIngredientsTextView.becomeFirstResponder()
        }
        else if self.recipeIngredientsTextView.text?.count == 0 {
            AppHelper.showFailToast(message: "Ingredient must not be empty")
            self.recipeIngredientsTextView.becomeFirstResponder()
        }
        else if self.recipeStepsTextView.text?.count == 0 {
            AppHelper.showFailToast(message: "Steps must not be empty")
            self.recipeStepsTextView.becomeFirstResponder()
        }
        else {
            self.saveRecipeToRealm()
        }
    }

    private func setupView() {
        self.navigationItem.title = "Add Recipe"
        self.navigationController?.isNavigationBarHidden = false
        
        let addButton = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(self.onSaveButtonTapped))
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    private func saveRecipeToRealm() {
        let recipe = Recipe()
        let id = "\(Int.random(in: 1..<1000))"
        if !self.recipeRealm.contains(where: {$0.id == id}) {
            recipe.id = "\(id)"
        }
        else {
            AppHelper.showFailToast(message: "Recipe ID exist please try again.")
            return
        }
        recipe.title = self.recipeTitleTextField.text
        recipe.category = self.recipeCategoryTextField.text
        recipe.ingredient = self.recipeIngredientsTextView.text
        recipe.steps = self.recipeStepsTextView.text
        AppHelper.saveImage(imageName: recipe.id!, image: self.recipeImageView.image!)
        recipe.imageURL = recipe.id
        AppHelper.insertObjectToRealm(fromObject: recipe)
        NotificationCenter.default.post(name: .onNewRecipeAdded, object: self)
        let alertController = UIAlertController(
            title: "CONGRATS",
            message: "Successfully added recipe!",
            preferredStyle: UIAlertController.Style.alert
        )
        let confirmAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.navigationController?.popViewController(animated: true)
            })
        }
        alertController.addAction(confirmAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - UIImagePickerControllerDelegate
extension AddRecipeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage {
            self.recipeImageView.image = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
