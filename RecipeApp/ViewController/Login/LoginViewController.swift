//
//  LoginViewController.swift
//  RecipeApp
//
//  Created by Calvin on 29/05/2022.
//

import UIKit

class LoginViewController: UIViewController {

    /* =================================================================
     *                   MARK: - Outlet Initialization
     * ================================================================= */
    @IBAction func onContinueWithEmailButtonTapped(_ sender: Any) {
        let viewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "enterEmailViewController")
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    /* =================================================================
     *                   MARK: - Class Function
     * ================================================================= */
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.hidesBackButton = false
        self.navigationController?.isNavigationBarHidden = false
    }

}
