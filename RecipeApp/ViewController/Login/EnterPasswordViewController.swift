//
//  EnterPasswordViewController.swift
//  RecipeApp
//
//  Created by Calvin on 29/05/2022.
//

import UIKit

class EnterPasswordViewController: UIViewController {

    /* =================================================================
     *                   MARK: - Local Initialization
     * ================================================================= */
    private var loginViewModel = LoginViewModel()
    
    /* =================================================================
     *                   MARK: - Outlet Initialization
     * ================================================================= */
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBAction func onContinueButtonTapped(_ sender: Any) {
        if self.passwordTextField.text?.count == 0 {
            self.passwordTextField.becomeFirstResponder()
            AppHelper.showFailToast(message: "Please enter a valid password with a minimum length of 6")
        }
        else {
            if AppHelper.validatePassword(self.passwordTextField.text ?? "") {
                self.signIn()
            }
            else {
                self.passwordTextField.becomeFirstResponder()
                AppHelper.showFailToast(message: "Please enter a valid password with a minimum length of 6")
            }
        }
    }
    
    /* =================================================================
     *                   MARK: - Class Function
     * ================================================================= */
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    private func signIn() {
        self.showLoadingScreen()
        guard let email = UserDefaults.standard.string(forKey: Constant.UserDefaults.userDefaultsEmail.rawValue) else { return }
        self.loginViewModel.signIn(email: email, password: self.passwordTextField.text ?? "") { success, error in
            self.hideLoadingScreen()
            if success {
                let alertController = UIAlertController(
                    title: "WELCOME",
                    message: "Successfully sign in!",
                    preferredStyle: UIAlertController.Style.alert
                )
                let confirmAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { _ in
                    UserDefaults.standard.set(true, forKey: Constant.UserDefaults.userDefaultsIsLoggedIn.rawValue)
                    UserDefaults.standard.synchronize()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                        self.navigationController?.popToRootViewController(animated: true)
                    })
                }
                alertController.addAction(confirmAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else {
                AppHelper.showFailToast(message: error)
            }
        }
    }
    
}
