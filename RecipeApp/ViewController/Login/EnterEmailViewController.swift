//
//  EnterEmailViewController.swift
//  RecipeApp
//
//  Created by Calvin on 29/05/2022.
//

import UIKit
import FirebaseAuth

class EnterEmailViewController: UIViewController {

    /* =================================================================
     *                   MARK: - Local Initialization
     * ================================================================= */
    private var loginViewModel = LoginViewModel()
    
    /* =================================================================
     *                   MARK: - Outlet Initialization
     * ================================================================= */
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBAction func onContinueButtonTapped(_ sender: Any) {
        if self.emailTextField.text?.count == 0 {
            self.emailTextField.becomeFirstResponder()
            AppHelper.showFailToast(message: "Please enter a valid email")
        }
        else {
            if AppHelper.validateEmail(self.emailTextField.text ?? "") {
                self.checkEmailExist()
            }
            else {
                self.emailTextField.becomeFirstResponder()
                AppHelper.showFailToast(message: "Please enter a valid email")
            }
        }
    }
    
    /* =================================================================
     *                   MARK: - Class Function
     * ================================================================= */
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    private func checkEmailExist() {
        self.loginViewModel.validateEmail(email: self.emailTextField.text ?? "") { success in
            if success {
                UserDefaults.standard.set(self.emailTextField.text, forKey: Constant.UserDefaults.userDefaultsEmail.rawValue)
                UserDefaults.standard.synchronize()
                let viewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "enterPasswordViewController")
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else {
                UserDefaults.standard.set(self.emailTextField.text, forKey: Constant.UserDefaults.userDefaultsEmail.rawValue)
                UserDefaults.standard.synchronize()
                let viewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "registrationViewController")
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
}
